#!/usr/bin/env python3

def new_board():
    return [
        [None, None, None],
        [None, None, None],
        [None, None, None]
    ]


def render(board):
    print("  0 1 2")
    for i in range(0, len(board)):
        print("{}|".format(i), end="")
        for j in range(0, len(board[i])):
            value = board[i][j] if board[i][j] is not None else " "
            print(value, end=" ")
        print("|")


def is_board_full(board):
    for i in range(0, len(board)):
        for j in range(0, len(board[i])):
            if board[i][j] is None:
                return False

    return True


def update_board(board, move_coords, value):
    i, j = move_coords
    board[i][j] = value
    return board


def get_move(player):
    i = int(input("Player {}: What is your move's x coord?: ".format(player)))
    j = int(input("Player {}: What is your move's y coord?: ".format(player)))
    return (i, j)


def is_valid_move(board, move_coords):
    i, j = move_coords
    if(i >= 3 or j >= 3 or i < 0 or j < 0):
        return False
    if(board[i][j] is not None):
        return False

    return True


def make_move(board, player):
    while(True):
        move_coords = get_move(player)

        if is_valid_move(board, move_coords):
            break

        print("Invalid move, try again!")

    return update_board(board, move_coords, player)


def update_player(player):
    return 'O' if player == 'X' else 'X'


def get_winner(board):
    # first row
    player_move_move = board[0][0]
    if(board[0][1] == player_move_move and board[0][2] == player_move_move):
        return player_move_move

    # first col
    player_move_move = board[0][0]
    if(board[1][0] == player_move_move and board[2][0] == player_move_move):
        return player_move_move

    # second row
    player_move = board[1][0]
    if(board[1][1] == player_move and board[1][2] == player_move):
        return player_move

    # second col
    player_move = board[0][1]
    if(board[1][1] == player_move and board[2][1] == player_move):
        return player_move

    # third row
    player_move = board[2][0]
    if(board[2][1] == player_move and board[2][2] == player_move):
        return player_move

    # third col
    player_move = board[0][2]
    if(board[1][2] == player_move and board[2][2] == player_move):
        return player_move

    # first diagonal
    player_move = board[0][0]
    if(board[1][1] == player_move and board[2][2] == player_move):
        return player_move

    # second diagonal
    player_move = board[0][2]
    if(board[1][1] == player_move and board[2][0] == player_move):
        return player_move

    return None


def is_draw(board):
    return is_board_full(board)


def start_game():
    board = new_board()
    current_player = 'X'

    while(True):
        render(board)

        board = make_move(board, current_player)

        winner = get_winner(board)

        current_player = update_player(current_player)

        if winner is not None:
            print("Winner is player {}".format(winner))
            break

        if is_draw(board):
            print("IT'S A DRAW!!")
            break


if __name__ == "__main__":
    start_game()
