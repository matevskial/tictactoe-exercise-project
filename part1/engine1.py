#!/usr/bin/env python3

class Board(object):
    def __init__(self, n=3):
        self.size = n
        self.board = []
        for i in range(0, n):
            self.board.append([])
            for j in range(0, n):
                self.board[i].append(None)
    
    def render(self):
        print(" ", end="")
        for i in range(0, self.size):
            print(" {}".format(i), end="")
        print("")
        for i in range(0, self.size):
            print("{}|".format(i), end="")
            for j in range(0, self.size):
                value = self.board[i][j] if self.board[i][j] is not None else " "
                print(value, end=" ")
            print("|")

    def get_cell(self, i, j):
        return self.board[i][j]

    def update_cell(self, i, j, value):
        self.board[i][j] = value

    def is_board_full(self):
        for i in range(0, len(self.board)):
            for j in range(0, len(self.board[i])):
                if self.board[i][j] is None:
                    return False
        return True


class TicTacToe(object):
    def __init__(self):
        self.board = Board()
        self.player = 'X'

    def get_move(self):
        i = int(input("Player {}: What is your move's x coord?: ".format(self.player)))
        j = int(input("Player {}: What is your move's y coord?: ".format(self.player)))
        return (i, j)

    def is_valid_move(self, move_coords):
        i, j = move_coords
        if(i >= 3 or j >= 3 or i < 0 or j < 0):
            return False
        if(self.board.get_cell(i, j) is not None):
            return False

        return True

    def make_move(self):
        while(True):
            move_coords = self.get_move()

            if self.is_valid_move(move_coords):
                break

            print("Invalid move, try again!")

        self.update_board(move_coords)

    def update_board(self, move_coords):
        i, j = move_coords
        value = self.player
        self.board.update_cell(i, j, value)

    def update_player(self):
        if(self.player == 'X'):
            self.player = 'O'
        else:
            self.player = 'X'
    
    def get_winner(self):
        n = self.board.size
        for i in range(0, n):
            # check ith row
            player_move = self.board.get_cell(i, 0)
            for j in range(0, n):
                if(self.board.get_cell(i, j) != player_move):
                    break
                if(j == n - 1 and player_move is not None):
                    return player_move

            # check ith col
            player_move = self.board.get_cell(0, i)
            for j in range(0, n):
                if(self.board.get_cell(j, i) != player_move):
                    break
                if(j == n - 1 and player_move is not None):
                    return player_move
        
        # check first diagonal
        player_move =self.board.get_cell(0, 0)
        for i in range(0, n):
            if(self.board.get_cell(i, i) != player_move):
                break
            if(i == n - 1 and player_move is not None):
                return player_move

        # check second diagonal
        player_move = self.board.get_cell(0, 2)
        for i in range(0, n):
            if(self.board.get_cell(i, n-i-1) != player_move):
                break
            if(i == n - 1 and player_move is not None):
                return player_move

        # no winner is found
        return None

    def is_draw(self):
        return self.board.is_board_full()

    def start_game(self):
        while(True):
            self.board.render()

            self.make_move()

            winner = self.get_winner()

            self.update_player()

            if winner is not None:
                print("Winner is player {}".format(winner))
                break

            if self.is_draw():
                print("IT'S A DRAW!!")
                break


if __name__ == "__main__":
    game = TicTacToe()
    game.start_game()
