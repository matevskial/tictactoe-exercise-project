## Part1

Part one is the implementation of the actual command-line Tic-Tac-Toe game playable by the user.

I implemented the game in two ways:

- The same structure from the [article](https://robertheaton.com/2018/10/09/programming-projects-for-advanced-beginners-3-a/)

- Using classes

# Implementation of engine1.py

Here I created two classes.

The `Board` class is used to represend the Tic-Tac-Toe board. The class has methods to render the board, check the value of a cell, update the value of a cell and check if the board is full

The `TicTacToe` class is used to control the actual game. In this class are implemented the methods to start the game, check for a win or for a draw and get the input from the user.

# Implementation of engine2.py

It is implemented only with several function defined in the linked article.

The `start_game` function starts the game and checks whether the game is draw or someone has won and exits in that situation.

The empty Tic-Tac-Toe board is represented as `[[None, None, None], [None, None, None], [None, None, None]]`

