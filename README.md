## Introduction

This is my implementation of the programming project Tic-Tac-Toe taken from [here](https://robertheaton.com/2018/10/09/programming-projects-for-advanced-beginners-3-a/).

The linked article explains how to start and structure the project.
It is helpful for building programs as exercise.

## Instructions to run the game for part 1

- Clone the repo

- Open the command line in the directory `part1`

- type `python3 engine1.py` to run the first implementation

- type `python3 engine2.py` to run the second implementation
